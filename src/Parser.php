<?php
namespace Caridpars;

class Parser
{
	private $array_data 	= [];
	private $array_ids		= [];
	private $titles			= [];

	function __construct($pdo,$phpexcel) {
		$this->phpexcel = $phpexcel;
	}

	public function start()
	{
		$content = $this->fileGetContentz("https://www.groundforce.com/lowering-fitment-tool.php");
		$years = $this->between('<option value=','>', $content);
		array_pop($years);
		array_pop($years);
		array_shift($years);

		$iplist = array('185.112.12.34','185.112.12.35','185.112.13.183','185.112.14.43','185.112.15.39','185.112.15.149','212.86.111.18','212.86.111.21');

		foreach ($years as $year) {
			$searchtype = "lowering-kit-search/";
			$request_obj = "findMake";
			$searchtypeyear = "?year=";
			$searchtypemake = '';
			$make = '';
			$model = '';
			$ip = $iplist[rand(0, 7)];
			$make_content = $this->postrequestrs($request_obj, $year, $make, $model, $searchtype, $searchtypeyear, $searchtypemake, $ip);
			$makes = $this->between('<option value="','">', $make_content);
			array_shift($makes);

			foreach ($makes as $make) {
				$searchtype = "lowering-kit-search/";
				$request_obj = "findModel";
				$searchtypemmy = "?year=";
				$searchtypemake = '&make=';
				$model = '';
				$ipp = $iplist[rand(0, 7)];
				$model_content = $this->postrequestrs($request_obj, $year, $make, $model, $searchtype, $searchtypemmy, $searchtypemake, $ipp);
				$models = $this->between('<option value="','">', $model_content);
				array_shift($models);
				foreach ($models as $model) {
					$f = fopen("process_links_mmy", 'a+');
					fwrite($f, date("F j, Y, H:i:s") . " - " . $year . "-" . $make . "-" . $model . "\n");
					fclose($f);
					$array_data['year'] = $year ;
					$array_data['make'] = $make ;
					$array_data['model'] = $model ;
					$searchtype = "";
					$request_obj = "show_lowering_kit_results";
					$searchtypemmy = '';
					$searchtypemake = '';
					$ip = $iplist[rand(0, 7)];
					$product_content = $this->postrequestrsfinal($request_obj, $year, $make, $model, $searchtype, $searchtypemmy, $searchtypemake, $ip);
					$fitable = $this->between('<table class="display">', '</table>', $product_content);
					$fitabletr = $this->between('<tr>', '</tr>', $fitable[0]);
					if ($fitable[0] == "") {
						$f = fopen("proxy_error_links", 'a+');
						fwrite($f, date("F j, Y, H:i:s") . " - " . $year . "-" . $make . "-" . $model .'-'.$ip."\n");
						fclose($f);
					}
					array_shift($fitabletr);
					foreach ($fitabletr as $fitabletrsingl) {
						$fitabletd = $this->between('<td>', '</td>', $fitabletrsingl);
						$forrepl = $fitabletd[6];
						$pos = strpos($forrepl, '<a');
						$pos1 = strpos($forrepl, '">');
						$dataid = substr($forrepl, $pos, $pos1 - $pos);
						$dataid = str_replace('<a href="part-details.php?part=', "", $dataid);
						$array_data['part'] = $dataid;
						$array_data['notes'] = $fitabletd[3];
						$array_data['type'] = $fitabletd[7];
						$array_data['make'] = $fitabletd[1];
						$array_data['model'] = $fitabletd[2];
						$array_data['year'] = $fitabletd[0];
						$array_data['cab'] = $fitabletd[5];
						$array_data['drivetrain'] = $fitabletd[4];
						$array_data['fdrop'] = $fitabletd[8];
						$array_data['rdrop'] = $fitabletd[9];
						$array_data_allfin[] = $array_data;
						$f = fopen("process_links", 'a+');
						fwrite($f, date("F j, Y, H:i:s") . " - " . $year . "-" . $make . "-" . $model . "-".$array_data['part']."\n");
						fclose($f);
					}
				}
			}
		}
		$new_arr = array();
		foreach ($array_data_allfin as $array_data_a) {
			$new_arr[] = implode('|',$array_data_a);
		}
		$result = array_unique($new_arr);
		$resreversarr = array();
		foreach ($result as $resrevers) {
			$resreversarr[] = explode('|',$resrevers);
		}
//Загружаем все спарсеное в файл
		$titles['title'] = array('Year(s)','Make','Model','Part Number','Notes','Kit Type','Cab Size','Drivetrain','Front Drop','Rear Drop');
		$filename = "products";
		$this->array2csv($resreversarr, $titles, $filename);
		$this->phpexcelrun();
		print_r($array_data_allfin);die();
	}

	public function array2csv(array &$array, $titles, $filename) {
		if (count($array) == 0) {
			return null;
		}
		$df = fopen($filename.'.csv', 'w');
		foreach ($titles as $row) {
			fputcsv($df, $row, '|', chr(0));
		}
		foreach ($array as $row) {
			fputcsv($df, $row, '|', chr(0));
		}
		fclose($df);
		return ob_get_clean();

	}

	public function get_microtime() {
		list($usec, $sec) = explode(" ", microtime());
		return ((float)$usec + (float)$sec);
	}

	public function fileGetContentz($url){
		//sleep(rand (1 , 2));
		$cookieFile = __DIR__ . '/cookies.txt';
		$settings = array(
			CURLOPT_RETURNTRANSFER => true,
			// CURLOPT_FOLLOWLOCATION => true,
			//CURLOPT_MAXREDIRS      => 4,
			//CURLOPT_HEADER         => false,
			CURLOPT_SSL_VERIFYHOST => false,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_HTTPHEADER => array('Cookie: __utmt=1; __utma=83068352.146716984.1490271806.1490351561.1490358508.9; __utmb=83068352.9.10.1490358508; __utmc=83068352; __utmz=83068352.1490271806.1.1.utmcsr=(direct)|utmccn=(direct)|utmcmd=(none)', 'Accept: */*', 'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4', 'Content-Type: text/html', 'Referer: https://www.groundforce.com/lowering-fitment-tool.php'),
			//CURLOPT_TIMEOUT        => 10,
			CURLOPT_USERAGENT      => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.8) Gecko/2009032609 Firefox/3.0.8',
			CURLOPT_COOKIEFILE => $cookieFile,
			CURLOPT_COOKIEJAR => $cookieFile,
//			CURLOPT_PROXY => '185.112.12.34',
//			CURLOPT_PROXYPORT => 2831,
//			CURLOPT_PROXYTYPE => "HTTP",
//			CURLOPT_PROXYUSERPWD => 'robotkos:KXoGz6dA',
//			CURLOPT_HTTPPROXYTUNNEL => true,
			#CURLOPT_COOKIEJAR => '/logs/cookie'.$this->tubeRn.'.tmp'
		);
		ini_set('max_execution_time', 10000);
		$socket = curl_init($url);
		curl_setopt_array($socket, $settings);
		$a = curl_exec($socket);

		return $a;

	}
	public function postrequestrs ($request_obj, $year, $make, $model, $searchtype, $searchtypemmy, $searchtypemake, $ip) {
		sleep(rand (1 , 3));
		$cookieFile = __DIR__ . '/cookies.txt';
		$myCurl = curl_init();
		if ($request_obj == "findMake") {$arrayy = array(
			'year' => $year
		);
		}
		if ($request_obj == "findModel") {
			$arrayy = array(
				'year' => $year,
				'make' => $make
			);
		}
		if ($request_obj == "show_lowering_kit_results") {$arrayy = array(
			'year' => $year	,
			'make' => $make,
			'model' => $model
		);}
		curl_setopt_array($myCurl, array(
			CURLOPT_URL => 'https://www.groundforce.com/search/'.$searchtype.$request_obj.'.php'.$searchtypemmy.$year.$searchtypemake.$make,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.8) Gecko/2009032609 Firefox/3.0.8',
			CURLOPT_SSL_VERIFYHOST => false,
			CURLOPT_SSL_VERIFYPEER => false,
			//CURLOPT_TIMEOUT => 1,
			CURLOPT_COOKIEFILE => $cookieFile,
			CURLOPT_COOKIEJAR => $cookieFile,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_HTTPHEADER => array('X-Requested-With: XMLHttpRequest'),

			CURLOPT_PROXY => $ip,
			CURLOPT_PROXYPORT => 2831,
			CURLOPT_PROXYUSERPWD => 'robotkos:KXoGz6dA',
			CURLOPT_HTTPPROXYTUNNEL => true,

			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => http_build_query($arrayy)
		));
		// Получили года
		//echo $i." \n";
		//$i+=10;
		$response = curl_exec($myCurl);
		//echo empty($response)." la la \n";
		curl_close($myCurl);
		return $response;
	}
	public function postrequestrsfinal ($request_obj, $year, $make, $model, $searchtype, $searchtypemmy, $searchtypemake, $ip) {
		sleep(rand (2 , 4));
		$cookieFile = __DIR__ . '/cookies.txt';
		$myCurl = curl_init();
		if ($request_obj == "findMake") {$arrayy = array(
			'year' => $year
		);
		}
		if ($request_obj == "findModel") {
			$arrayy = array(
				'year' => $year,
				'make' => $make
			);
		}
		if ($request_obj == "show_lowering_kit_results") {$arrayy = array(
			'year' => $year	,
			'make' => $make,
			'model' => $model
		);}

		curl_setopt_array($myCurl, array(
			CURLOPT_URL => 'https://www.groundforce.com/search/'.$searchtype.$request_obj.'.php',
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.8) Gecko/2009032609 Firefox/3.0.8',
			CURLOPT_SSL_VERIFYHOST => false,
			CURLOPT_SSL_VERIFYPEER => false,
			//CURLOPT_TIMEOUT => 1,
			CURLOPT_COOKIEFILE => $cookieFile,
			CURLOPT_COOKIEJAR => $cookieFile,
			CURLOPT_CUSTOMREQUEST => 'POST',
			CURLOPT_HTTPHEADER => array('X-Requested-With: XMLHttpRequest'),

			CURLOPT_PROXY => $ip,
			CURLOPT_PROXYPORT => 2831,
			CURLOPT_PROXYUSERPWD => 'robotkos:KXoGz6dA',
			CURLOPT_HTTPPROXYTUNNEL => true,

			CURLOPT_POST => true,
			CURLOPT_POSTFIELDS => http_build_query($arrayy)
		));
		// Получили года
		//echo $i." \n";
		//$i+=10;
		$response = curl_exec($myCurl);
		//echo empty($response)." la la \n";
		curl_close($myCurl);
		return $response;
	}
	public function between($start, $end, $content)
	{

		preg_match_all('~' . $start . '(.*?)' . $end . '~is', $content, $result);

		return $result[1];

	}
	public function phpexcelrun()
	{
		$upOne = realpath(__DIR__ . '/../../../');
		$this->phpexcel->objReader = \PHPExcel_IOFactory::createReader('CSV');
// If the files uses a delimiter other than a comma (e.g. a tab), then tell the reader
		$this->phpexcel->objReader->setDelimiter("|");
// If the files uses an encoding other than UTF-8 or ASCII, then tell the reader
		$this->phpexcel->objReader->setInputEncoding('UTF-8');
		$this->phpexcel->objPHPExcel = $this->phpexcel->objReader->load($upOne.'/products.csv');
		$this->phpexcel->objWriter = \PHPExcel_IOFactory::createWriter($this->phpexcel->objPHPExcel, 'Excel5');
		$this->phpexcel->objWriter->save("Groundforce_task_".date("m-d-y").'.xls');
	}

	public function unique_multidim_array($array, $key) {
		$temp_array = array();
		$i = 0;
		$key_array = array();

		foreach($array as $val) {
			if (!in_array($val[$key], $key_array)) {
				$key_array[$i] = $val[$key];
				$temp_array[$i] = $val;
			}
			$i++;
		}
		return $temp_array;
	}

}
?>